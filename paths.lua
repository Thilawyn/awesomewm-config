local lua_version   = _VERSION:match("^Lua (.+)$")
local luarocks_tree = os.getenv("HOME") .. "/.config/awesome/rocks"


package.path = luarocks_tree .. "/share/lua/" .. lua_version .. "/?.lua;" ..
               luarocks_tree .. "/share/lua/" .. lua_version .. "/?/init.lua;" ..
               package.path

package.cpath = luarocks_tree .. "/lib/lua/" .. lua_version .. "/?.so;" ..
                package.cpath
