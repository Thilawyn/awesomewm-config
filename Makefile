LUA_VERSION   = 5.1

LUAROCKS      = luarocks --lua-version=$(LUA_VERSION) --tree $(LUA_TREE)
LUAROCKS_TREE = rocks


install-rocks: clean-rocks
	# $(LUAROCKS) install connman_widget

clean-rocks:
	$(RM) -r $(LUAROCKS_TREE)
